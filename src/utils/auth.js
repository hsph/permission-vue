import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

// netty相关
const NettyTokenKey = 'Netty-Token'

export function getNettyToken() {
  return Cookies.get(NettyTokenKey)
}

export function setNettyToken(token) {
  return Cookies.set(NettyTokenKey, token)
}

export function removeNettyToken() {
  return Cookies.remove(NettyTokenKey)
}
