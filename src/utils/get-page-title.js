import defaultSettings from '@/settings'

const title = defaultSettings.title || 'momo-cloud-permission'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
