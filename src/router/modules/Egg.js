import Layout from '@/layout'

const eggRouter = {
  path: '/momo/egg',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Egg',
  meta: {
    title: '彩蛋篇',
    icon: 'egg_line',
    roles: ['MMP']
  },
  children: [
    {
      path: 'egg',
      component: () => import('@/views/momo/egg/page/Publish'),
      name: 'Publish',
      meta: { title: '即将发布', noCache: true, roles: ['MMP'] }
    },
    {
      path: 'project',
      component: () => import('@/views/momo/egg/page/Project'),
      name: 'Project',
      meta: { title: '规划中', noCache: true, roles: ['MMP'] }
    }
  ]
}

export default eggRouter
