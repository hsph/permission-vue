import Layout from '@/layout'

const userRouter = {
  path: '/momo/author',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Auth',
  meta: {
    title: '权限管理',
    icon: 'table',
    roles: ['MMP']
  },
  children: [
    {
      path: 'userList',
      component: () => import('@/views/momo/user/UserList'),
      name: 'AuthorUser',
      meta: { title: '用户管理', noCache: true, roles: ['MMP'] }
    },
    {
      path: 'aclTree',
      component: () => import('@/views/momo/author/AclTree'),
      name: 'AclTree',
      meta: { title: '菜单管理', noCache: true, roles: ['MMP'] }
    },
    {
      path: 'roleList',
      component: () => import('@/views/momo/role/RoleList'),
      name: 'AuthorRole',
      meta: { title: '角色管理', noCache: true, roles: ['MMP'] }
    },
    {
      path: 'enterprise',
      component: () => import('@/views/momo/enterprise/SysEnterprise'),
      name: 'SysEnterpriseList',
      meta: { title: '企业管理', noCache: true, roles: ['MMP'] }
    },
    {
      path: 'enterprise/roleList/:uuid',
      component: () => import('@/views/momo/enterprise/roleComponent/SysEnterpriseRoleList'),
      name: 'SysEnterpriseRoleList',
      hidden: true,
      meta: { title: '企业角色列表', noCache: true, roles: ['MMP'] }
    },
    {
      path: 'enterprise/userList/:uuid',
      component: () => import('@/views/momo/enterprise/userComponent/SysEnterpriseUserList'),
      name: 'SysEnterpriseUserList',
      hidden: true,
      meta: { title: '企业用户列表', noCache: true, roles: ['MMP'] }
    }
  ]
}

export default userRouter
