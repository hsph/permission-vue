#项目跟路径 --save 无论什么环境
# 解决移动端300ms点击延迟问题
npm install fastclick --save
#辅助初css
npm install stylus --save
npm install stylus-loader --save
#websocket长链接
npm install sockjs-client
npm install stompjs　　

#better-scroll
npm install better-scroll --save

#安装轮播图插件
npm install vue-awesome-swiper@2.6.7 --save

#ajax
npm install --save axios
#vuex 数据共享
npm install vuex --save
#动画库
npm install animate.css --save
#cookie
npm install vue-js-cookie
npm install js-cookie --save

#将对象序列化，多个对象之间用&拼接（拼接是由底层处理，无需手动操作）
#将序列化的内容拆分成一个个单一的对象
npm install qs --save
#图片浏览组件v-viewer，支持旋转、缩放、翻转等操作
npm install v-viewer
npm install --save babel-polyfill
npm install --save fuse.js
npm install --save echarts
 npm install --save normalize.css
cnpm install echarts -S
npm i svgo svg-sprite-loader -D
#vue工程，scss文件依赖失败解决方案
npm install --save-dev node-sass
npm install --save node-sass
npm install --save-dev sass-loader
npm install

npm install --save screenfull vue-count-to vue-i18n

#按需安装
npm i vant -S

#使用render-content不显示树形组件问题解决方法
npm install babel-plugin-syntax-jsx babel-plugin-transform-vue-jsx babel-helper-vue-jsx-merge-props babel-preset-env --save-dev
